using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Service : MonoBehaviour //script to check if the service is ok (requested beer vs innkeeper beer)
{

    public GameObject associatedClient; //Used to link with the client in front of the bar

    private void OnTriggerEnter(Collider other) //When the innkeeper is in front of the client, we check if the client's request match with the handed beer.
    {
         if (PlayerControl.selectedBeer == associatedClient.GetComponent<Chair>().beerRequested)
        {
            associatedClient.GetComponent<Chair>().ServiceOK(); //To be used later to trigger all the tasks linked with the service
            other.GetComponent<PlayerControl>().SelectBeer(4); //refesh the innkeeper handed beer
            other.GetComponent<PlayerControl>().HideBeer();  
        }
    }

}
