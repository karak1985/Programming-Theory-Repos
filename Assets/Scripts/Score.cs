using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class Score : MonoBehaviour //script to display the score once the game is over depending of the score
{
    [SerializeField]
    private TextMeshProUGUI scroreText;

    void Start()
    {
        if(GameEngine.score < 0)
        {
            scroreText.text = "Hey " + MainMenu.playerName + " too bad...<br>" + GameEngine.score.ToString();
        }
        if (GameEngine.score >= 0 && GameEngine.score <= 100)
        {
            scroreText.text = "Hey " + MainMenu.playerName + " that was good<br>" + GameEngine.score.ToString();
        }
        if (GameEngine.score > 100)
        {
            scroreText.text = "Hey " + MainMenu.playerName + " that was AWESOME<br>" + GameEngine.score.ToString();
        }

    }

    public void Restart()
    {
        SceneManager.LoadScene(1, LoadSceneMode.Single); //reload the game
    }  
}
