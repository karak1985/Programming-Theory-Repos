using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Exit : MonoBehaviour //used to destroy clients when they leave the tavern
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<ClientBehaviour>().toDestroy == true) //if the client is going home, destoy it
        {
            Destroy(other.gameObject);
        }
    }
}
