﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class DrunkClient : ClientBehaviour // INHERITANCE child of clientBehaviour to add a new behaviour (2 beer requests per client)

{
    private float drunkTimer = 5.0f;
    public bool servedTwice = false;

    public override void Drink() // POLYMORPHISM
    {
        if (served == false)
        {
            StartCoroutine(DrinkRoutine()); //we start a coroutine to wait some time before asking the client to go back
            IEnumerator DrinkRoutine()
            {
                served = true;
                GameEngine.AddToScore();
                yield return new WaitForSeconds(drunkTimer);
                WaitForTheSecondOrder(10);
            }
        }
        else
        {
            StartCoroutine(DrinkRoutine()); //we start a coroutine to wait some time before asking the client to go back
            IEnumerator DrinkRoutine()
            {
                servedTwice = true;
                yield return new WaitForSeconds(drunkTimer);
                GameEngine.AddToScore();
                GoBack();
            }
        }


    }

    public void WaitForTheSecondOrder(int waitTimer) // Wait few seconds the order
    {
        DisplayRequest();
        StartCoroutine(MessageDelayRoutine()); //we start a coroutine to wait some time before hidding the request
        IEnumerator MessageDelayRoutine()
        {
            //We 2 sec before hidding the request
            yield return new WaitForSeconds(2);
            HideRequest();
            StartCoroutine(WaitRoutine()); //we start a coroutine to wait some time before asking the client to go back
        }
        IEnumerator WaitRoutine()
        {
            //We wait before going home
            yield return new WaitForSeconds(waitTimer);
            if (servedTwice == false)
            {
                GoBack();
                GameEngine.RemoveToScore();
            }
        }

    }

}
