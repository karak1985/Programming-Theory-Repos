using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;


public class GameEngine : MonoBehaviour //game engine to calculare score, set game timer...
{
    [SerializeField]
    private TextMeshProUGUI scoreText; //text to display the score
    [SerializeField]
    private TextMeshProUGUI timerText; //text to display the timer
    public static int score { get; private set; } = 0; // ENCAPSULATION
    public static int bonus { get; } = 5; // ENCAPSULATION can be modified later with difficulty
    public static int malus { get; } = 2; // ENCAPSULATION can be modified later with difficulty
    private float gameTime = 100f;

    private void Start()
    {
        score = 0;
    }

    private void Update()
    {
        scoreText.text = score.ToString(); //adding or removing score

        if (gameTime > 0) //timer decreasing from gameTime to 0, then End Game
        {
            gameTime -= Time.deltaTime;
            timerText.text = "Time : " + Mathf.Round(gameTime).ToString();
        }
        else
        {
            EndGame();
        }
    }

    public static void AddToScore() //function to call from anywhere to add score
    {
        score += bonus;
    }

    public static void RemoveToScore() //function to call from anywhere to remove score
    {
        score -= malus;
    }

    private void EndGame() //function to call the endGame and go back to Main menu
    {
        SceneManager.LoadScene(2, LoadSceneMode.Single);
    }
}
