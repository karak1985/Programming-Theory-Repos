using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour //main menu functions (for buttons)

    
{
    [SerializeField]
    private TextMeshProUGUI playerNameField;
    public static string playerName { get; private set; } = null; // to be used later to register best score./
    [SerializeField] 
    private Button playButton; //used to enable the Play button when a name is set.


    public void RegisterName() //function to register the name, can be used later for best score
    {
        playerName = playerNameField.text;
        playButton.interactable = true; //activate the play button to force a name

    }

    public void Play() //function for the button Play
    {
        SceneManager.LoadScene(1, LoadSceneMode.Single);
    }

    public void QuitGame() //function for the button Quit
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#endif
        Application.Quit();
    }
}
